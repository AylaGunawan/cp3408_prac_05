using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    GameObject id;

    public List<Edge> edgeList = new List<Edge>();
    public Node path = null; // next node in path?
    public Node cameFrom;

    public float f, g, h;

    public Node(GameObject i)
    {
        id = i;
        path = null;
    }

    public GameObject GetId() // to compare this node with other nodes
    {
        return id;
    }
}
