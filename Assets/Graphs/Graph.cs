using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph
{
    List<Edge> edges = new List<Edge>(); // list of all edges
    List<Node> nodes = new List<Node>(); // list of all nodes
    List<Node> pathList = new List<Node>(); // list of nodes populated by A* + backwards

    public Graph() { }

    public void AddNode(GameObject id)
    {
        Node node = new Node(id);
        nodes.Add(node);
    }

    public void AddEdge(GameObject fromNode, GameObject toNode)
    {
        Node from = FindNode(fromNode);
        Node to = FindNode(toNode);

        if (from != null && to != null) // check if from and to nodes exist
        {
            Edge e = new Edge(from, to);
            edges.Add(e);
            from.edgeList.Add(e);
        }
    }

    Node FindNode(GameObject id) // look for a node of an id in list of all nodes
    {
        foreach (Node n in nodes)
        {
            if (n.GetId() == id)
            {
                return n;
            }
        }
        return null;
    }

    public bool AStar(GameObject startId, GameObject endId)
    {
        Node start = FindNode(startId);
        Node end = FindNode(endId);

        if (start == null || end == null)
        {
            return false;
        }

        List<Node> open = new List<Node>();
        List<Node> closed = new List<Node>();
        float tentative_g = 0; // g score to give to node about to be put into a list + select as next node in graph
        bool tentative_is_better;

        // handle start
        start.g = 0;
        start.h = GetDistance(start, end);
        start.f = start.h;

        // handle search
        open.Add(start);
        while (open.Count > 0)
        {
            int i = GetLowestF(open); // find best node in open to close
            Node thisNode = open[i];
            if (thisNode.GetId() == endId)
            {
                ReconstructPath(start, end); // make backwards path
                return true;
            }

            open.RemoveAt(i);
            closed.Add(thisNode);

            // handle neighbour search
            Node neighbour;
            foreach (Edge e in thisNode.edgeList) // all edges that branch from the node that was just closed
            {
                neighbour = e.endNode;

                if (closed.IndexOf(neighbour) > -1) // if neighbour out of bounds (already visited), ignore
                {
                    continue;
                }

                tentative_g = thisNode.g + GetDistance(thisNode, neighbour);
                if (open.IndexOf(neighbour) == -1) // if neighbour not in open list, add to open list
                {
                    open.Add(neighbour);
                    tentative_is_better = true;
                } else if (tentative_g < neighbour.g) // if the last g is better than new neighbour g?
                {
                    tentative_is_better = true;
                } else
                {
                    tentative_is_better = false;
                }

                if (tentative_is_better)
                { // choose the neighbour?
                    neighbour.cameFrom = thisNode;
                    neighbour.g = tentative_g;
                    neighbour.h = GetDistance(thisNode, end);
                    neighbour.f = neighbour.g + neighbour.h;
                }
            }
        }
        return false; // there is no path between the 2 param nodes
    }

    public void ReconstructPath(Node startId, Node endId)
    {
        pathList.Clear();
        pathList.Add(endId); // add end to list

        var p = endId.cameFrom; // parent
        while (p != startId && p != null)
        {
            pathList.Insert(0, p); // add parent at the front of list
            p = p.cameFrom;
        }

        pathList.Insert(0, startId); // add start at the front of list
    }

    float GetDistance(Node a, Node b)
    {
        return (Vector3.SqrMagnitude(a.GetId().transform.position - b.GetId().transform.position));
    }

    int GetLowestF(List<Node> l)
    {
        float lowestF = 0;
        int count = 0;
        int iteratorCount = 0;

        lowestF = l[0].f;

        for (int i = 1; i < l.Count; i++)
        {
            if (l[i].f < lowestF) // not <= -> unecessary val swapping
            {
                lowestF = l[i].f;
                iteratorCount = count;
            }
            count++;
        }
        return iteratorCount; // (index of) node with lowest f
    }
}
