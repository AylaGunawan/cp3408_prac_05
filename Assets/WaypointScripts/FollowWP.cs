using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowWP : MonoBehaviour
{
    public GameObject[] waypoints;
    int currentWP = 0;

    public float distance = 5.0f; // minimum distance to current waypoint
    public float speed = 10.0f;
    public float rotateSpeed = 1.0f; // turning circle, too low -> never meets min dist to current waypoint
    public float trackerSpeedAdd = 2.0f; // at least as fast as speed, too fast -> followers will take shortcuts
    public float trackerDistance = 10.0f;

    GameObject tracker;
    float trackerSpeed;

    // Start is called before the first frame update
    void Start()
    {
        tracker = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        DestroyImmediate(tracker.GetComponent<Collider>());
        tracker.GetComponent<MeshRenderer>().enabled = false; // turn off tracker rendering
        tracker.transform.position = this.transform.position;
        tracker.transform.rotation = this.transform.rotation;

        trackerSpeed = speed + trackerSpeedAdd;
    }

    void ProgressTracker()
    {
        // do not move if tracker is too far ahead of tank
        if (Vector3.Distance(tracker.transform.position, this.transform.position) > trackerDistance)
        {
            return;
        }

        // target next waypoint only when current waypoint is reached
        if (Vector3.Distance(tracker.transform.position, waypoints[currentWP].transform.position) < distance)
        {
            currentWP++;
        }

        // target first waypoint if last waypoint is reached (avoid index error)
        if (currentWP >= waypoints.Length)
        {
            currentWP = 0;
        }

        // get perfect angle + move tracker
        tracker.transform.LookAt(waypoints[currentWP].transform);
        tracker.transform.Translate(0, 0, (speed + 2) * Time.deltaTime);
    }

    // Update is called once per frame
    void Update()
    {
        ProgressTracker();

        // get vector angle from tank location to tracker as quaternion
        Quaternion lookAtWP = Quaternion.LookRotation(tracker.transform.position - this.transform.position);

        // spherically interpolate rotation using angle over time at a speed
        this.transform.rotation = Quaternion.Slerp(transform.rotation, lookAtWP, rotateSpeed * Time.deltaTime);

        this.transform.Translate(0, 0, speed * Time.deltaTime);
    }
}
